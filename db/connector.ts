import { ConnectOptions, MongoClient } from "mongodb";

const options = {
  useUnifiedTopology: true,
  useNewUrlParser: true,
} as ConnectOptions;

let client;
let clientPromise: Promise<MongoClient>;

if (!process.env.MONGODB_CONNECTION_URL) throw new Error("Please specify a connection URL to env.local");

client = new MongoClient(process.env.MONGODB_CONNECTION_URL, options);
clientPromise = client.connect();

export default clientPromise;
