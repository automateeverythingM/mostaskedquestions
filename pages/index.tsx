import type { NextPage } from "next";
import Head from "next/head";
import { BigTitle, ShowIP } from "../src/components";
const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Most Asked Question</title>
        <meta name="description" content="Answering most asked question from experts." />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <>
        <BigTitle />
        <ShowIP />
      </>
    </>
  );
};

export default Home;
