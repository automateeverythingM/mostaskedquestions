import { nanoid } from "nanoid";
import NextAuth from "next-auth";
import Credentials from "next-auth/providers/credentials";
import GithubProvider from "next-auth/providers/github";
import GoogleProvider from "next-auth/providers/google";
import EmailProvider from "next-auth/providers/email";
import { MongoDBAdapter } from "@next-auth/mongodb-adapter";
import clientPromise from "../../../db/connector";
import { uniqueNamesGenerator, adjectives, names, colors } from "unique-names-generator";

export default NextAuth({
  session: {
    strategy: "jwt",
  },
  jwt: {
    secret: process.env.JSW_SECRET,
    maxAge: 60 * 60 * 24 * 7,
  },
  pages: {
    signIn:"/login",
    verifyRequest:"/login/verifyrequest",
  },
  providers: [
    GithubProvider({
      clientId: process.env.GITHUB_ID,
      clientSecret: process.env.GITHUB_SECRET,
    }),
    GoogleProvider({
      clientId: process.env.GOOGLE_ID as string,
      clientSecret: process.env.GOOGLE_SECRET as string,
    }),
    Credentials({
      id: "anonymous",
      name: "anonymous",
      credentials: {},
      async authorize() {
        return anonymousUser();
      },
    }),
    EmailProvider({
      name:"emailProvider",
      server:process.env.EMAIL_SERVER,
      from: process.env.EMAIL_FROM,
    }),
    Credentials({
      name: "emailpassword",
      credentials:{
        username:{},
        password:{}
      },
      async authorize(credentials, req) {

        return {credentials};
      },
    }),
  ],
  secret: process.env.SECRET_KEY,
  adapter: MongoDBAdapter(clientPromise),
});

const anonymousUser = () => {
  const name = uniqueNamesGenerator({ dictionaries: [adjectives, colors, names] });
  const id = nanoid();

  return { id, name };
};
