// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { getIpFromRequest } from "../../src/service/RequestHandlers";
import { generateResponse, IResponse } from "../../src/service/ResponseGenerate";

export default function handler(req: NextApiRequest, res: NextApiResponse<IResponse>) {
  const ip = getIpFromRequest(req);

  if (!ip) {
    return res.status(404).json({ ...generateResponse({ success: false, error: { message: "ip address not found", code: 404 } }) });
  }
  
  return res.status(200).json({ ...generateResponse({ data: { ip: ip } }) });
}
