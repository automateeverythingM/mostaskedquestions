import { NextPage } from "next";
import Link from "next/link";

const VerifyRequest: NextPage = () => {
  return (
    <div className="container flex py-20 mt-28 w-auto flex-col items-center justify-center">
      <div>Please check your email</div>
      <div>A sign in link has been sent to your email address.</div>
      <Link href="/">
        <a className="link">Go Home</a>
      </Link>
    </div>
  );
};

export default VerifyRequest;
