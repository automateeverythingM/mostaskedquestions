import { GetServerSideProps } from "next";
import { ClientSafeProvider, getCsrfToken, getProviders, getSession } from "next-auth/react";
import Head from "next/head";
import { LoginForm } from "../../src/components";

interface ILoginProps {
  provider: ClientSafeProvider | undefined;
  csrfToken: string | undefined;
}

const Login = ({provider, csrfToken}: ILoginProps) => {
  return (
    <div>
      <Head>
        <title>Most Asked Question | Login</title>
      </Head>

      <div className="f_center pt-24">
        <LoginForm provider={provider} csrfToken={csrfToken} />
      </div>
    </div>
  );
};

export default Login;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { req, res } = ctx;
  
  const providers = await getProviders();
  const emailProvider = providers?.email;
  const csrfToken = await getCsrfToken();


  return {
    props: {
      provider: emailProvider,
      csrfToken: csrfToken,
    },
  };
};