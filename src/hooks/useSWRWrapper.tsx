import useSWR from "swr";
import { fetcher } from "./fetcher";

export interface ISWRResponse {
  data: any;
  isLoading: boolean;
  error: any;
}

export const useSWRWrapper = (url: string) => {
  const { data, error } = useSWR(url, fetcher);

  return {
    data,
    isLoading: !error && !data,
    isError: error,
  };
};
