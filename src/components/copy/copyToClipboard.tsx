import clsx from "clsx";
import { IoCopyOutline } from "react-icons/io5";

interface ICopyIPProps {
  customAction?: Function;
  show?: boolean;
  text?: string;
}

export const CopyToClipboard = ({ customAction, text, show }: ICopyIPProps) => {
  return (
    <div
      onClick={() => {
        customAction?.();
        if (text) navigator.clipboard.writeText(text);
      }}
      className={clsx("copy_container", {"opacity-0": show})}
    >
      <IoCopyOutline className="copy_outline" />
    </div>
  );
};
