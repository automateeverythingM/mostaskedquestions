import clsx from "clsx";
import { useState } from "react";
import { useSWRWrapper } from "../../hooks/useSWRWrapper";
import { CopyToClipboard } from "../copy/copyToClipboard";
import { ToolTip } from "../toolTip/toolTip";

export const ShowIP = () => {
  const { data: response, isError, isLoading } = useSWRWrapper("/api/getip");

  const { data, success, error } = response ?? {};

  return (
    <>
      <div className="ip_sub">Its your ip</div>
      <div className="ip_container">
        {isLoading ? (
          <div className="ip">...</div>
        ) : (
          <>
            <span className="ip">{data?.ip || "123.45.231"}</span>
            <ToolTip text="COPIED" showTime={2000}>
              <CopyToClipboard text={data?.ip || "123.45.231"} />
            </ToolTip>
          </>
        )}
      </div>
    </>
  );
};
