interface IBackgroundProps {}

export const Background = ({}: IBackgroundProps) => {
  return (
    <div className="animated_background">
      <div className="flex-1"></div>
      <div className="flex-1"></div>
      <div className="flex-1"></div>
      <div className="flex-1"></div>
      <div className="flex-1"></div>
    </div>
  );
};
