import clsx from "clsx";
import React, { ReactElement, useEffect, useState } from "react";

interface IToolTipProps {
  text: string;
  showTime: number;
  children: ReactElement;
}

export const ToolTip = ({ text, children, showTime }: IToolTipProps) => {
  const [show, setShow] = useState(false);

  useEffect(() => {
    let id: NodeJS.Timeout;
    if (show) {
      id = setTimeout(() => {
        setShow(false);
      }, showTime);
    }
    return () => clearTimeout(id);
  }, [show, showTime]);

  return (
    <div className="tooltip_container">
      <div className={clsx("tooltip_text", !show && "hidden")}>{text}</div>
       {React.cloneElement(children, { customAction: () => setShow(true), show: show })}
    </div>
  );
};
