import { ClientSafeProvider, signIn, useSession } from "next-auth/react";
import Link from "next/link";
import router from "next/router";
import { useForm } from "react-hook-form";
import { BsGithub } from "react-icons/bs";
import { FcGoogle } from "react-icons/fc";
import { ButtonWithIcon } from "../buttons/buttonWithIcon";
interface ILoginProps {
  provider: ClientSafeProvider | undefined;
  csrfToken: string | undefined;
}

type Inputs = {
  email: string;
  password: string;
};

export const LoginForm = ({ provider, csrfToken }: ILoginProps) => {
  const {
    register,
    watch,
    trigger,
    getValues,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();

  const { data: session, status } = useSession();

  const signInPasswordLess = async () => {
    const validEmail = await trigger("email", { shouldFocus: true });
    const email = getValues().email;
    if (validEmail) signIn("email", { email });
  };

  const isPasswordSet = !!watch().password;
  if(status === "loading" || status === "unauthenticated") <div>LOADING</div>
  if (session) router.replace("/");

  const onSubmit = (data: Inputs) => {
    if (!data.password) return signIn("email", { email: data.email });
    signIn("credentials", { email: data.email, password: data.password });
  };

  return (
    <div className="form_container">
      <div className="title">Login</div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <input type="hidden" name="csrf_token" defaultValue={csrfToken} />
        <div className="input_group">
          <label htmlFor="email">Email</label>
          <input
            autoComplete="off"
            className="input"
            id="email"
            type="email"
            {...register("email", { required: "please enter email", pattern: { value: /.+@.+\..+/, message: "Please enter a valid email" } })}
          />
          <span className="error">{errors.email?.message}</span>
        </div>

        <div className="input_group">
          <label htmlFor="password">Password</label>
          <input className="input" id="password" type="password" {...register("password")} />
          <span className="error">{errors.password?.message}</span>
        </div>

        <Link href="/resetpassword">
          <a className="link">Forgot Password ?</a>
        </Link>
        <div>
          <button disabled={!isPasswordSet} type="submit" className="w-full btn btn-black dark_mode_blue mb-5">
            Login
          </button>
          <button onClick={signInPasswordLess} disabled={isPasswordSet} className="w-full btn btn-black dark_mode_blue mb-5">
            PasswordLess Login
          </button>
        </div>

        <div className="providers">
          <ButtonWithIcon onClick={() => signIn("github")} icon={<BsGithub size="2rem" />} className="btn-black w-full">
            Github
          </ButtonWithIcon>
          <ButtonWithIcon onClick={() => signIn("google")} icon={<FcGoogle size="2rem" />} className="btn-black w-full">
            Google
          </ButtonWithIcon>
        </div>

        <div className="text-center mt-5">
          <button className="link" onClick={() => signIn("anonymous")}>
            Anonymous Login
          </button>
        </div>
      </form>
    </div>
  );
};
