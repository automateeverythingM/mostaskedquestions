import { useSession } from "next-auth/react";
import { useRouter } from "next/router";

interface IProtectedRouteProps {
  protectedRoutes: string[];
  children: JSX.Element;
}

export const ProtectedRoute = ({ protectedRoutes, children }: IProtectedRouteProps) => {
  const router = useRouter();
  const { data: session, status } = useSession();


  const pathIsProtected = !!protectedRoutes.find((r) => router.pathname.startsWith(r));
  if(!pathIsProtected) return children;

  const isLoading = status === "loading";
  const isAuthenticated = status === "authenticated";


  if (!isLoading && !isAuthenticated && pathIsProtected) {
    router.push("/login");
    return null;
  }

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return children;
};


