import { Footer, Navbar } from "..";
import { Background } from "../background/background";
import { ProtectedRoute } from "../routes/protectedRoute";
interface ILayoutProps {
  children: React.ReactNode;
}
export const Layout = ({ children }: ILayoutProps) => {
  const toggleTheme = () => {
    document.body.classList.toggle("dark");
  };
  return (
    <div className="layout">
      <Background />
      <Navbar toggleTheme={toggleTheme} />
      <ProtectedRoute protectedRoutes={["/explore", ""]}>
        <main>{children}</main>
      </ProtectedRoute>
      <Footer />
    </div>
  );
};
