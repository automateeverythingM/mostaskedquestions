import Link from "next/link";
import Image from "next/image";
import { useSession, signOut } from "next-auth/react";
interface INavbarProps {
  toggleTheme: () => void;
}

export const Navbar = ({ toggleTheme }: INavbarProps) => {
  const { data: session } = useSession();
  return (
    <div className="navbar_container">
      <div>
        <nav className="nav">
          <div className="logo_container">
            <Link href="/">
              <a className="logo">WHA</a>
            </Link>
          </div>

          <ul className="nav_links">
            <li>
              <Link href="/explore">
                <a>Explore</a>
              </Link>
            </li>
            <li>
              <Link href="/register">
                <a>Register</a>
              </Link>
            </li>
            <li>
              {session ? <button onClick={() => signOut()}>Logout</button> :
                <Link href="/login">
                  <a>Login</a>
                </Link>
              }
            </li>
          </ul>
        </nav>
      </div>
      <div className="text-center">
        <button onClick={toggleTheme} className="theme_toggler">
          <Image src="/assets/images/test-card.png" alt="Paintroller" height="80px" width="80px" layout="responsive" />
        </button>
      </div>
    </div>
  );
};
