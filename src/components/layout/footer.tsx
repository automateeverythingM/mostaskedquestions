import Link from "next/link";
interface IFooterProps {}

export const Footer = ({}: IFooterProps) => {
  return (
    <footer className="footer">
      <div>
        <Link href="/">
          <a>Footer</a>
        </Link>
      </div>
      <div></div>
    </footer>
  );
};
