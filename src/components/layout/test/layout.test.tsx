import { render, screen } from "@testing-library/react";

import { Layout } from "../layout";
describe("layout", () => {
  test("should render children", () => {
    render(
      <Layout>
        <div>Some Child</div>
      </Layout>
    );
    expect(screen.getByText(/some ch/i)).toBeInTheDocument();
  });

  test("should render navbar", () => {
    const doc = render(
      <Layout>
        <div>Some Child</div>
      </Layout>
    );

    expect(doc.container.querySelector("nav")).toBeTruthy();
  });

  test("should render footer", () => {
    const doc = render(
      <Layout>
        <div>Some Child</div>
      </Layout>
    );
    expect(doc.container.querySelector("footer")).toBeTruthy();
  });
});
