interface IBigTitleProps {}

export const BigTitle = ({}: IBigTitleProps) => {
  return (
    <div className="header_container">
      <div className="big_title">WE HAVE ANSWER</div>
      <div className="bit_title_subtitle">TO GOOGLE MOST ASKED QUESTION</div>
    </div>
  );
};
