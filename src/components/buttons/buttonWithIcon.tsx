import clsx from "clsx";
import { HTMLAttributes, memo } from "react";

interface IButtonWithIconProps extends HTMLAttributes<HTMLButtonElement> {
    icon: React.ReactNode;
    children: React.ReactNode;
    className?: string;
}

// eslint-disable-next-line react/display-name
export const ButtonWithIcon = ({ icon, children, className, ...rest }: IButtonWithIconProps) => {
    return (
        <button {...rest} className={clsx("btn btn-icon", className)}>
            <span className="icon">{icon}</span>
            <span className="content">{children}</span>
        </button>
    );
};
