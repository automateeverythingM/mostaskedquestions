import { NextApiRequest } from "next";

export const getIpFromRequest = (req: NextApiRequest) => {
  const tryRealIP = req.headers["x-real-ip"];

  return tryRealIP || req.headers.forwarded;
};
