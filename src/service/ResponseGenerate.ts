export interface IResponse {
  success: boolean;
  data: any;
  error: {
    message?: string;
    code?: number;
  };
}

const defaultResponse = {
  success: true,
  data: [],
  error: {},
};

export const generateResponse = (data: Partial<IResponse>): IResponse => {
  return { ...defaultResponse, ...data };
};
