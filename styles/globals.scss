@tailwind base;
@tailwind components;
@tailwind utilities;

$footer-height: 5rem;
$nav-height: 16rem;
$nav-height-sm: 22rem;

$yellow: #b99f36;
$red: #a1361e;
$blue: #2a6c88;
$green: #818842;
$purple: #6e3877;

@layer utilities {
  .dark_text_drop_shadow {
    .dark & {
      filter: drop-shadow(0.1em 0.1em #000);
    }
  }
  .dark_text_drop_shadow_small {
    .dark & {
      filter: drop-shadow(0.05em 0.05em #000);
    }
  }

  .nav_height_regular {
    min-height: calc(100vh - $nav-height);
  }
  .nav_height_sm {
    min-height: calc(100vh - $nav-height-sm);
  }

  .f_center {
    @apply flex items-center justify-center;
  }
}

body {
  font-family: "Sigmar One", cursive;
}

input:-webkit-autofill {
  box-shadow: inset 0 0 0px 9999px white;
  -webkit-box-shadow: inset 0 0 0px 9999px white;
}

.btn {
  @apply text-center px-3 py-3 rounded-md text-inherit f_center transition-all;
}

.btn-icon {
  @apply flex py-0;
  .icon {
    @apply px-5 py-2 border-r-4;
  }
  .content {
    @apply flex-1;
  }
}

.btn-black {
  @apply bg-black text-white mt-3 text-sm;
  &:hover {
    @apply bg-neutral-900;
  }
  &:active {
    @apply bg-neutral-800;
  }

  &:disabled {
    @apply bg-neutral-600;
  }
}

.dark_mode_red {
  .dark & {
    background: $red;
  }
}

.dark_mode_blue {
  .dark & {
    background: $blue;
  }
}

.footer {
  height: $footer-height;
  @apply absolute inset-x-0 bottom-0;
}

.layout {
  padding-bottom: $footer-height;
  @apply relative min-h-screen h-full dark:text-gray-50;
}

//? Verify email

.notification

//? Verify email



//? NaV
.navbar_container {
  height: 22rem;
  @apply sm:h-64;
}

.nav {
  @apply container transition-all flex flex-wrap sm:flex-nowrap py-5 font-bold justify-between items-center text-lg xs:text-2xl;
}

.logo {
  @apply f_center rounded-full bg-black text-white h-24 w-24 mx-3 dark:border-2;
}

.logo_container {
  @apply flex w-full sm:w-auto justify-center;
}

.nav_links {
  @apply f_center w-full sm:w-auto py-10 sm:py-0;

  & li {
    @apply mr-5  dark_text_drop_shadow;
  }

  & li a:hover {
    @apply text-neutral-700 dark:text-white;
  }
}

.theme_toggler {
  @apply w-16 h-16 xs:w-20 xs:h-20 sm:h-28 sm:w-28 dark:grayscale transition-colors ease-in-out duration-500;
  &:active {
    animation: shakeOdd 0.5s;
  }
}

//? NaV

//? BigTitle
.header_container {
  @apply flex flex-col sm:items-center sm:justify-center sm:nav_height_regular nav_height_sm;
}

.big_title {
  @apply text-4xl xs:text-6xl sm:text-8xl md:text-9xl font-bold text-center px-10 dark_text_drop_shadow;

  // mix-blend-mode: difference;
}

.bit_title_subtitle {
  @apply text-center text-sm sm:text-2xl dark_text_drop_shadow;
}
//? BigTitle

//? IP
.ip_container {
  @apply text-3xl sm:text-6xl md:text-7xl lg:text-8xl text-center relative flex justify-center;
}

.ip {
  @apply  dark_text_drop_shadow_small  transition-all ease-in-out;
}

.ip_sub {
  @apply text-3xl sm:text-4xl md:text-7xl lg:text-8xl text-center dark_text_drop_shadow_small;
}


//? IP

//?Copy

.copy_container {
  @apply f_center ml-2;
}

.copy_outline {
  @apply text-black dark:text-white  dark_text_drop_shadow_small cursor-pointer active:text-neutral-700 active:dark_text_drop_shadow;
}

//?Copy

//? Tooltip
.tooltip_container {
  @apply relative f_center z-10;
}

.tooltip_text {
  @apply absolute dark:text-white dark:bg-opacity-0 dark_text_drop_shadow_small animate-ping;
}

//? Tooltip

//? Animated Background
.animated_background {
  @apply min-h-screen flex absolute inset-0;
  z-index: -1;
  & div {
    @apply transition-all transform rotate-90 origin-top-right;
  }

  .dark & div:nth-child(odd) {
    animation: shakeOdd 0.5s;
  }
  .dark & div:nth-child(even) {
    animation: shakeEven 0.5s;
  }

  .dark & div:nth-child(1) {
    background: $yellow;
    @apply rotate-0 border-x-2;
  }
  .dark & div:nth-child(2) {
    background: $red;
    @apply rotate-0 border-x-2;
  }
  .dark & div:nth-child(3) {
    background: $blue;
    @apply rotate-0 border-x-2;
  }
  .dark & div:nth-child(4) {
    background: $green;
    @apply rotate-0 border-x-2;
  }
  .dark & div:nth-child(5) {
    background: purple;
    @apply rotate-0 border-x-2;
  }
}
//? Animated Background

//? Forms
.form_container {
  @apply py-2 xs:pt-8 xs:pb-4 px-4 xs:px-8 w-11/12 sm:w-auto border-4 sm:border-8 border-black rounded-2xl shadow-black bg-white text-black;

  form {
    @apply flex flex-col w-full sm:w-96;
  }

  .title {
    @apply text-xl xs:text-2xl;
  }

  .input_group {
    @apply flex flex-col pt-4 text-sm xs:text-xl;

    label {
      @apply pl-1;
    }

    .error {
      @apply pl-1 text-stone-400 dark:text-red-900 h-4;
    }

    .input {
      @apply py-2 px-2 border-4 border-neutral-400 rounded-md outline-none bg-white;
      &:focus {
        @apply border-black shadow-inner shadow-gray-600;
      }
    }
  }

  .link {
    @apply mt-2 mb-8 text-neutral-600 text-sm xs:text-xl w-auto;

    &:hover {
      @apply underline text-black;
    }
  }
}



//? Forms

@keyframes shakeOdd {
  0% {
    transform: translate(-1px, -2px) rotate(-1deg);
  }
  10% {
    transform: translate(1px, 1px) rotate(0deg);
  }
  20% {
    transform: translate(3px, 2px) rotate(0deg);
  }
  30% {
    transform: translate(-3px, 0px) rotate(1deg);
  }
  40% {
    transform: translate(-1px, 2px) rotate(-1deg);
  }
  50% {
    transform: translate(1px, -1px) rotate(1deg);
  }
  60% {
    transform: translate(3px, 1px) rotate(-1deg);
  }
  70% {
    transform: translate(-3px, 1px) rotate(0deg);
  }
  80% {
    transform: translate(1px, 2px) rotate(0deg);
  }
  90% {
    transform: translate(-1px, -1px) rotate(1deg);
  }
  100% {
    transform: translate(1px, -2px) rotate(-1deg);
  }
}
@keyframes shakeEven {
  0% {
    transform: translate(1px, 1px) rotate(0deg);
  }
  10% {
    transform: translate(-1px, -2px) rotate(-1deg);
  }
  20% {
    transform: translate(-3px, 0px) rotate(1deg);
  }
  30% {
    transform: translate(3px, 2px) rotate(0deg);
  }
  40% {
    transform: translate(1px, -1px) rotate(1deg);
  }
  50% {
    transform: translate(-1px, 2px) rotate(-1deg);
  }
  60% {
    transform: translate(-3px, 1px) rotate(0deg);
  }
  70% {
    transform: translate(3px, 1px) rotate(-1deg);
  }
  80% {
    transform: translate(-1px, -1px) rotate(1deg);
  }
  90% {
    transform: translate(1px, 2px) rotate(0deg);
  }
  100% {
    transform: translate(1px, -2px) rotate(-1deg);
  }
}
